-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 25, 2018 at 01:56 AM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.32-2+0~20181015120817.7+stretch~1.gbpa6b8cf

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ta`
--

-- --------------------------------------------------------

--
-- Table structure for table `lookup`
--

CREATE TABLE `lookup` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

CREATE TABLE `tbl_comment` (
  `id` int(11) NOT NULL,
  `content` text,
  `status` tinyint(4) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `author` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `komen_id` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE `tbl_post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `tags` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL,
  `author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_post`
--

INSERT INTO `tbl_post` (`id`, `title`, `content`, `tags`, `status`, `create_time`, `update_time`, `author_id`) VALUES
(1, 'judul', '&lt;p&gt;konten&lt;/p&gt;', 'tag', NULL, '2018-12-23 12:29:38', NULL, 7),
(2, 'judul', '&lt;p&gt;konten&lt;/p&gt;', 'tag', NULL, '2018-12-23 12:30:06', NULL, 7),
(3, 'React atau Vue ?', '&lt;p&gt;Penasaran dengan Framework JavaScript pilihan saya?&lt;/p&gt;&lt;p&gt;Kenapa cuma &lt;strong&gt;&lt;em&gt;React&lt;/em&gt;&lt;/strong&gt; dan &lt;strong&gt;&lt;em&gt;Vue&lt;/em&gt;&lt;/strong&gt;? &lt;strong&gt;&lt;em&gt;Angular&lt;/em&gt;&lt;/strong&gt;-nya mana?&lt;/p&gt;&lt;p&gt;Kenapa &lt;em&gt;ga&lt;/em&gt; sekalian &lt;em&gt;Preact&lt;/em&gt;, &lt;em&gt;Inferno&lt;/em&gt;, &lt;em&gt;Svelte&lt;/em&gt;, atau framework-framework lainnya?&lt;/p&gt;&lt;p&gt;Berikut ini akan saya ceritakan kisah perkenalan dan hubungan saya dengan berbagai framework JavaScript yang pernah saya coba, sekaligus framework mana yang akhirnya menjadi pilihan saya. Metode pencariannya kira-kira masih serupa dengan kisah saya dalam tulisan &lt;strong&gt;&lt;u&gt;Menemukan Pasangan Hidup yang Tepat.&lt;/u&gt;&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;Latar Belakang&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;Baru-baru ini saya pindah bidang pekerjaan dari Backend ke Frontend. Salah satu alasan utamanya adalah semakin pesatnya perkembangan JavaScript selama beberapa tahun terakhir, mulai dari lahirnya NodeJS sampai dengan munculnya berbagai framework seperti Angular, React, &amp;amp; Vue. Keempat framework tersebut adalah pemain &lt;em&gt;pemain besar&lt;/em&gt; di kancah per-JavaScript-an internasional. Oleh karena itulah saya coba pelajari keempatnya.&lt;/p&gt;&lt;p&gt;&lt;strong&gt;&lt;em&gt;Empat? Angular + Vue + React = 4?&lt;/em&gt;&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;Di sini Angular saya hitung 2, karena ada AngularJS (Angular 1.x) &amp;amp; dan Angular(Angular 2 dst.) hehe.. keduanya saya anggap berbeda karena memang keduanya benar-benar framework yang berbeda, baik dar segi API maupun implementasinya. Source code Angular 2 benar-benar ditulis ulang dari nol.&lt;/p&gt;&lt;p&gt;&lt;strong&gt;React -&amp;gt; AngularJS -&amp;gt; Angular 2 -&amp;gt; React&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;Sekitar 2,5 tahun yang lalu, saya mulai (serius) belajar JavaScript. Pada awalnya saya tertarik untuk mempelajari React terlebih dahulu karena &quot;katanya&quot; bagus, tapi saya merasa sangat kesulitan dalam mempelajarinya, karena waktu itu pemahaman saya mengenai JavaScript masih sangat dangkal. Terlebih lagi dokumentasi React saat itu masih berantakan, sehingga sulit untuk diikuti oleh pemula --sekarang sudah lebih rapi, meskipun tidak serapi Vue, hehe..&lt;/p&gt;&lt;p&gt;Waktu itu saya masih belum tahu kalau ada Vue, karena memang belum begitu terkenal, sehingga alternatif lain yang saya tahu hanya Angular.&lt;/p&gt;', 'tag', NULL, '2018-12-24 20:04:22', NULL, 7),
(4, 'Judul Artikel 1 Lindis', '&lt;p&gt;isi Artikel Lindis Edited 5 kali&lt;/p&gt;', 'tag', NULL, '2018-12-24 20:33:20', '2018-12-25 00:05:40', 2),
(5, 'Judul Artikel 2 Lindis', '&lt;p&gt;konten 2&lt;/p&gt;', 'tag', NULL, '2018-12-24 20:41:34', NULL, 2),
(7, 'Judul Post 1 Airi', '&lt;p&gt;Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati provident aliquid blanditiis repudiandae, tempora impedit. Voluptates rerum porro, odio est explicabo similique ipsam dolorem debitis laborum, sint quibusdam iste asperiores.&lt;/p&gt;', 'tag', NULL, '2018-12-25 01:17:07', NULL, 8),
(8, 'Judul Post 2 Airi edited 1 kali', '&lt;p&gt;Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati provident aliquid blanditiis repudiandae, tempora impedit. Voluptates rerum porro, odio est explicabo similique ipsam dolorem debitis laborum, sint quibusdam iste asperiores.&lt;/p&gt;', 'tag', NULL, '2018-12-25 01:17:56', '2018-12-25 01:18:49', 8);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tag`
--

CREATE TABLE `tbl_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `frequency` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `profile` varchar(255) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `bio` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `email`, `profile`, `nama`, `bio`) VALUES
(1, 'nellakharisma', '123456', 'nellakharisma@email.com', 'nellakharisma.jpg', 'nella kharisma', NULL),
(2, 'lindisofvalor', '123456', 'lindis@email.com', 'lindis.jpg', 'lindis', 'lindis bio'),
(5, 'kokbisa', '123456', 'kokbisa@email.com', 'kokbisa.jpg', 'kokbisa', NULL),
(6, 'coba', '123456', 'coba@email.com', 'nellakharisma.jpg', 'coba', NULL),
(7, 'jimyferi', '123456', 'jimyferi10@gmail.com', 'e69c51951aa2eaf2c33c6db71233cc26.jpg', 'jimmy feriawan', NULL),
(8, 'airi', '123456', 'airi@email.com', '22ad09cf0f48dfe23363877a063114e8.jpeg', 'airi', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lookup`
--
ALTER TABLE `lookup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `komen_id` (`komen_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_id` (`author_id`);

--
-- Indexes for table `tbl_tag`
--
ALTER TABLE `tbl_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lookup`
--
ALTER TABLE `lookup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_post`
--
ALTER TABLE `tbl_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_tag`
--
ALTER TABLE `tbl_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD CONSTRAINT `tbl_comment_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `tbl_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD CONSTRAINT `tbl_post_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
