-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 18, 2018 at 04:48 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tugas_akhir`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(32) CHARACTER SET utf8 NOT NULL,
  `nama` varchar(32) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL,
  `picture` varchar(100) CHARACTER SET utf8 NOT NULL,
  `profile` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `nama`, `email`, `password`, `picture`, `profile`) VALUES
('kokbisa', 'Kok Bisa', 'kokbisa@email.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'kokbisa.jpg', 'Akun resmi Kok Bisa Channel\r\nFakta menarik setiap hari\r\n#StayCurious\r\n? Business: kokbisachannel@gmail.com\r\nKok Bisa : Debunking Hoax the Series\r\n⬇⬇\r\nlinktr.ee/senyawa.plus'),
('nellakharisma', 'NELLA KHARISMA', 'nellakharisma@email.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'nellakharisma.jpg', '? @nk_second\r\nCP : 082143138402 / 085806511128\r\nFANBASE @nellalovers_id\r\nPP/ENDORSE : WA 082220000120 \r\n@kaos_nellakharisma WA 085735123000\r\nYOUTUBE⬇\r\nbit.ly/2Tvjxcs'),
('viavallen', 'Via Vallen', 'viavallen@email.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'viavallen.jpg', '☎️CP Off Air : +628123-544-3200/YANTO (chat by WA)\r\n?? brand ambassador : shopee & ertos\r\n#meraihbintang ⬇️\r\nbit.ly/MeraihBintangAsianGames2018');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `profil` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `nama`, `email`, `password`, `foto`, `profil`, `created_at`, `updated_at`) VALUES
(1, 'viavallen', 'Via Vallen', 'viavallen@email.com', 'password', 'viavallen.jpg', 'profil via vallen', '2018-12-08 09:37:54', NULL),
(2, 'nellakharisma', 'Nella Kharisma', 'nellakharisma@email.com', 'password', 'nellakharisma.jpg', 'profil nella kharisma', '2018-12-08 09:37:54', NULL),
(4, 'jimyferi', 'jimmy feriawan', 'jimyferi@email.com', 'password', 'jimmy.jpg', 'profil', '2018-12-08 11:46:04', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
