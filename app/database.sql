CREATE TABLE `users` (
    `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `username` varchar(32) NOT NULL UNIQUE,
    `nama` varchar(50) NOT NULL,
    `email` varchar(50) NOT NULL,
    `password` varchar(100) NOT NULL,
    `foto` varchar(100) NOT NULL,
    `profil` TEXT,
    `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` timestamp NULL DEFAULT NULL
);

CREATE TABLE `posts` (
    `post_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `user_id` int(11) DEFAULT NULL,
    `title` varchar(255) NOT NULL,
    `image` varchar(255) NOT NULL,
    `body` text NOT NULL,
    `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` timestamp NULL DEFAULT NULL,
    FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE ON ACTION ON UPDATE NO ACTION
);

INSERT INTO `users` (`username`, `nama`, `email`, `password`, `foto`, `profil`) values ("viavallen", "Via Vallen", "viavallen@email.com", "password", "viavallen.jpg", "profil via vallen"),
("nellakharisma", "Nella Kharisma", "nellakharisma@email.com", "password", "nellakharisma.jpg", "profil nella kharisma");