<?php

    require_once 'Database.php';

    class Artikel extends Database {
        public function editArtikel($artikelID, $artikelJudul, $artikelKonten) {
            $tgl = date('Y-m-d H:i:s');
            $sql = 'UPDATE tbl_post SET title="'. $artikelJudul .'", content="'. $artikelKonten .'", update_time="'. $tgl .'" WHERE id='. $artikelID;
            // return $sql;
            $query = $this->conn->query($sql);
            if($query) {
                return true;
            } else {
                return false;
            }
        }

        public function deleteArtikel($artikelID, $authorID) {
            $isUserAuthorized =  $this->isAuthorized($artikelID, $authorID);
            $id = (int) $artikelID;
            
            $status = false;
            
            if($isUserAuthorized) {
                $sql = 'DELETE FROM tbl_post WHERE id='. $id;
                $query = $this->conn->query($sql);
                if($query) {
                    $status = true;
                } else {
                    $status = false;
                }
            } else {
                $status = false;
            }
            return $status;
        }

        public function isAuthorized($artikelID, $authorID) {
            $artikel = $this->selectArtikelById($artikelID);
            // return $artikel;
            $idArtikel = $artikel['author_id'];
            if($idArtikel == $authorID) {
                $isAuthorized = true;
            } else {
                $isAuthorized = false;
            }
            return $isAuthorized;
        }

        public function createPost($judul, $konten, $user) {
            $konten = $this->conn->real_escape_string($konten);
            $judul = $this->conn->real_escape_string($judul);
            $tag = 'tag';
            $status = 1;
            $sql = 'insert into tbl_post (title, content, tags, author_id) VALUES("'. $judul .'", "'. $konten .'", "'. $tag .'", '. $user .')';
            // return $sql;
            if($query = $this->conn->query($sql)) {
              return true;
            } else {
              return false;
            }
          }
      
          public function getAllArtikel() {
              $kolom = 'tbl_post.id, tbl_post.content, tbl_post.create_time, tbl_post.title, tbl_user.profile';
              $sql = 'SELECT '. $kolom .' FROM tbl_post INNER JOIN tbl_user ON tbl_post.author_id=tbl_user.id';
              $query = $this->conn->query($sql);
              $data = array();
              $_temp = array();
              if($query->num_rows > 0) {
                  while($row = $query->fetch_assoc()) {
                      $_temp['id'] = $row['id'];
                      $_temp['content'] = $row['content'];
                      $_temp['create_time'] = $row['create_time'];
                      $_temp['title'] = $row['title'];
                      $_temp['profile'] = $row['profile'];
                      array_push($data,$_temp);
                      $_temp = array();
                  }
              } else {
                  $data = false;
              }
              return $data;
          }
      
          public function selectArtikelById($id) {
              $id = (int) $id;
              $kolom = 'tbl_post.author_id, tbl_post.id, tbl_post.title, tbl_post.content, tbl_post.create_time, tbl_user.profile, tbl_user.nama';
              $sql = 'SELECT '. $kolom .' FROM tbl_post INNER JOIN tbl_user ON tbl_post.author_id=tbl_user.id WHERE tbl_post.id='. $id .'';
              // return $sql;
              $data = array();
              $query = $this->conn->query($sql);
              if($query->num_rows > 0) {
                  while($hasil = $query->fetch_assoc()){
                      $data['id'] = $hasil['id'];
                      $data['author_id'] = $hasil['author_id'];
                      $data['content'] = $hasil['content'];
                      $data['profile'] = $hasil['profile'];
                      $data['create_time'] = $hasil['create_time'];
                      $data['title'] = $hasil['title'];
                      $data['nama_author'] = $hasil['nama'];
                  }
              } else {
                  $data = false;
              }
              return $data;
          }

          public function selectArtikelByAuthorId($author_id) {
            // $authorID = (int) $authorID;
            $where = 'WHERE tbl_post.author_id=' . $author_id;
            $kolom = 'tbl_post.author_id, tbl_post.id, tbl_post.title, tbl_post.content, tbl_post.create_time, tbl_user.nama';
            $sql = 'SELECT '. $kolom .' FROM tbl_post INNER JOIN tbl_user ON tbl_post.author_id=tbl_user.id '. $where;
            // return $sql;
            $data = array();
            $_temp = array();
            $query = $this->conn->query($sql);
            if($query->num_rows > 0) {
                while($hasil = $query->fetch_assoc()){
                    $_temp['id'] = $hasil['id'];
                    $_temp['author_id'] = $hasil['author_id'];
                    $_temp['content'] = $hasil['content'];
                    $_temp['create_time'] = $hasil['create_time'];
                    $_temp['title'] = $hasil['title'];
                    $_temp['nama'] = $hasil['nama'];
                    array_push($data, $_temp);
                    $_temp = array();
                }
            } else {
                $data = false;
            }
            return $data;
            return false;
          }
          
          public function getKomenArtikel($post_id) {
              return false;
          }
          
    }