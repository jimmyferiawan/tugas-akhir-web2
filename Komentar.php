<?php

    require_once 'Database.php';

    class Komentar extends Database {
        public function addKomen($artikelID, $userID, $isiKomen) {
            $post_id = (int) $artikelID;
            $author = (int) $userID;
            // $konten = $isiKomen;
            $konten = htmlspecialchars($isiKomen);
            // $konten = str_replace("\n", "&lt;br&gt;", $konten);
            $sql = 'INSERT INTO tbl_comment (content, author, post_id) VALUES ("'. $konten .'", '. $author .', '. $post_id .')';
            $query = $this->conn->query($sql);
            if($query) {
                return true;
            } else {
                return false;
            }
        }

        // cek apakah suatu komentar milik user yang login ? komentar bisa diedit : tidak bisa diedit
        public function cekKomenMilikUser($userID, $komenAuthor){
            $isAuthorized = $userID == $komenAuthor ? true : false;
            return $isAuthorized;
        }

        public function deleteKomen($komenID, $artikelID, $userID) {
            $sql = '';
        }

        public function editKomen($komenID, $userID, $isiKomen) {
            $id = (int) $komenID;
            $author = (int) $userID;
            $content = htmlspecialchars($isiKomen);
            // $sql = 'SELECT author from tbl_comment WHERE id='. $id;
            // $query = $this->conn->query($sql);
            // $pemilik = '';
            // if($query->num_rows > 0) {
            //     $pemilik = $query;
            // } else {
            //     return false;
            // }
            // return $pemilik;
            // $statusKepemilikan = $this->cekKomenMilikUser();
            $sql = 'UPDATE tbl_comment SET content="'. $content .'" WHERE id='. $id;
            $query = $this->conn->query($sql);
            if($query) {
                return true;
            } else {
                return false;
            }
        }
        public function getKomenByArtikelID($artikelID) {
            $_temp = array();
            $post_id = (int) $artikelID;
            $kolom = 'tbl_comment.id, tbl_comment.content, tbl_comment.status, tbl_comment.create_time, tbl_comment.author, tbl_comment.post_id, tbl_user.username, tbl_user.profile ';
            $sql = 'SELECT '. $kolom .' FROM tbl_comment INNER JOIN tbl_user ON tbl_comment.author=tbl_user.id WHERE post_id='. $post_id;
            // return $sql;
            $query = $this->conn->query($sql);
            $data = array();
            if($query){
                while($row = $query->fetch_assoc()) {
                    $_temp['id'] = $row['id'];
                    $_temp['content'] = $row['content'];
                    $_temp['status'] = $row['status'];
                    $_temp['create_time'] = $row['create_time'];
                    $_temp['author'] = $row['author'];
                    $_temp['post_id'] = $row['post_id'];
                    $_temp['username'] = $row['username'];
                    $_temp['profile'] = $row['profile'];
                    array_push($data, $_temp);
                    $_temp = array();
                }
                return $data;
            } else {
                return false;
            }
        }
    }