<?php
    session_start();
    if(isset($_SESSION['username'])) {
        header('Location: http://localhost/tugas-akhir/user/');
        exit;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>index</title>
    <link rel="stylesheet" href="css/materialize.css">
    <link rel="stylesheet" href="http://localhost/material-icons/iconfont/material-icons.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/materialize.min.js"></script>
</head>
<body>
    <div class="navbar-fixed">
        <nav class="nav-extended gradient-1">
            <div class="container nav-wrapper">
                <a href="#" class="brand-logo left">SI-POST</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down black-text">
                    <li><a href="https://gitlab.com/jimmyferiawan/tugas-akhir-web2">source code in gitlab</a></li>
                </ul>
            </div>
        </nav>
    </div>

    <div class="row">
        <div class="col m2 l4 xl4"></div>
        <div class="card col s12 m8 l4 xl4 no-padding">

            <div class="card-tabs">
                <ul class="tabs tabs-fixed-width">
                    <li class="tab"><a href="#login" class="deep-purple-text text-darken-3">Login</a></li>
                    <li class="tab"><a href="#register" class="deep-purple-text text-darken-3">Register</a></li>
                </ul>
            </div>
            <div class="divider"></div>
            <div class="card-content">
                <div id="login" class="">
                    <form action="user/user-login.php" method="post" id="form-log">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">person</i>
                            <input type="text" id="username-log" name="username" required>
                            <label for="username">username</label>
                            <span class="helper-text" data-error="* email kosong"></span>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">lock</i>
                            <input type="password" id="password-log" name="password" required>
                            <label for="password" >password</label>
                            <span class="helper-text" data-error="*password kosong"></span>
                        </div>
                        <button type="submit" name="login" class="btn center full-width deep-purple darken-1">Login</button>
                    </form>

                </div>
            
                <div id="register" class="row">
                    <form action="user/user-register.php" method="post" id="form-reg" enctype="multipart/form-data">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">mood</i>
                            <input type="text" id="nama-reg" name="nama">
                            <label for="nama">nama</label>
                            <span class="helper-text" data-error="nama kosong"></span>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">person</i>
                            <input type="text" id="username-reg" name="username">
                            <label for="username">username</label>
                            <span class="helper-text" data-error="username kosong"></span>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">email</i>
                            <input type="email" id="email-reg" name="email">
                            <label for="email">email</label>
                            <span class="helper-text" data-error="email kosong"></span>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">lock</i>
                            <input type="password" id="password-reg">
                            <label for="password">password</label>
                            <span class="helper-text" data-error="password kosong"></span>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">lock</i>
                            <input type="password" id="password-reg-retype" name="password" >
                            <label for="password">konfirmasi password</label>
                            <span class="helper-text" data-error="konfirmasi password kosong"></span>
                        </div>
                        <div class="file-field input-field col s12">
                            <i class="material-icons prefix"></i>
                            <div class="btn deep-purple darken-1">
                                <i class="material-icons">image</i>
                                <input type="file" id="profile" name="profile" accept="image/png,image/jpeg">
                            </div>
                            <div class="file-path-wrapper">
                                <input type="text" class="file-path validate" placeholder="Upload foto profil" readonly>
                            </div>
                            <span class="helper-text" data-error="email kosong"></span>
                        </div>
                        <button type="submit" name="register" class="col s12 btn full-width deep-purple darken-1">Register</button>
                    </form>
                    
                </div>
                    <?php
                        if(isset($_GET['status'])) {
                            
                            $status = (int) $_GET['status'];
                            $availableStatus = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
                            if(in_array($status, $availableStatus)) {
                                switch($status){
                                    case 1:
                                        $message = 'Berhasil Daftar silahkan login';
                                        $color = 'green';
                                        break;
                                    case 2:
                                        $message = 'Berhasil Hapus';
                                        $color = 'green';
                                        break;
                                    case 3:
                                        $message = 'Berhasil Logout';
                                        $color = 'green';
                                        break;
                                    case 4:
                                        $message = 'Gagal Daftar';
                                        $color = 'red';
                                        break;
                                    case 5:
                                        $message = 'Gagal Hapus';
                                        $color = 'red';
                                        break;
                                    case 6:
                                        $message = 'Gagal Logout';
                                        $color = 'red';
                                        break;
                                    case 7:
                                        $message = 'Username atau Password salah';
                                        $color = 'red';
                                        break;
                                    case 8:
                                        $message = 'Berhasil delete akun';
                                        $color = 'red';
                                        break;
                                    default:
                                        $message = '';
                                        $color = 'red';
                                        break;
                                }
                                echo '<span class="'. $color .'-text">'. $message .'</span>';
                            }
                            
                            
                        }
                    ?>
            </div>
        </div>
    </div>
<script src="js/index.js"></script>
</body>
</html>