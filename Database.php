<?php
  define('HOST', 'localhost');
  define('USER', 'jimmyferiawan');
  define('PASS', 'jimyferi');
  define('DB_NAME', 'ta');

  class Database {
    private $host = HOST;
    private $user = USER;
    private $pass = PASS;
    private $db_name = DB_NAME;
    public $conn;
    public $err;

    protected $sql;
    protected $query;

    public function __construct() {
      $this->conn = new mysqli($this->host, $this->user, $this->pass, $this->db_name);
      if(!$this->conn) {
        $this->err = 'Error: '. $this->conn->conect_error();
        return false;
      }
    }

    public function close() {
      $this->conn->close();
    }

    // database query CRUD
    public function select($col, $table_name) {
      $this->sql = 'select '.$this->splitColumn($col) . ' FROM '. $table_name;
      $this->query = $this->conn->query($this->sql);
      return $this->fetchData($col, $this->query); 
    }

    protected function splitColumn($col) {
      /**
       * memisahkan array menjadi string agar bisa 
       * dieksekusi pada query memilih kolom mysql
       */

      $splitted = '';

      for ($i=0; $i < count($col); $i++) { 
        $op = '';
        if($i == count($col)-1) {
          $op = '';
        } else {
          $op = ', ';
        }
        $splitted = $splitted . $col[$i] . $op;
      }
      return $splitted;
    }

    protected function fetchData($col, $query) {
      $data = array();

      if($query) {
        if($query->num_rows > 0) {
          while($row = $query->fetch_assoc()) {
            $_temp = array();
            
            foreach ($col as $value) {
              $_temp[$value] = $row[$value];
            }

            array_push($data, $_temp);
          }
        }
      }

      return $data;
    }

    public function insert($col, $table_name, $input) {
      $input_data = '';
      for($i=0;$i<count($input);$i++) {
        $koma='';
        if($i == count($input)-1) {
          $koma = '';
        } else {
          $koma = ', ';
        }
        $input_data = $input_data . '"'. $input[$i] .'"'. $koma;
      }
      $this->sql = 'INSERT INTO ' . $table_name . '('. $this->splitColumn($col) .') VALUES ('. $input_data .')';
      $this->query = $this->conn->query($this->sql);
      // echo '<br>'.$this->sql;
    }

  }