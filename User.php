<?php
  require 'Database.php';

  class User extends Database {
    private $table = 'tbl_user';
    private $logged_in = false;

    public function updateSession($id, $username, $email, $nama, $bio) {
        // session_start();
        $_SESSION['user_id'] = $id;
        $_SESSION['username'] = $username;
        $_SESSION['email'] = $email;
        $_SESSION['nama'] = $nama;
        $_SESSION['bio'] = $bio;
        // $_SESSION['username'] = $username;
    }

    public function updateSessionProfile($namaPhoto) {
        $_SESSION['profile'] = $namaPhoto;
    }

    public function selectById($col, $table_name, $id) {
        $where = 'WHERE id='.$id;
        $this->sql = 'select '.$this->splitColumn($col) . ' FROM '. $table_name . ' ' .$where;
        // return $this->sql;
        $this->query = $this->conn->query($this->sql);
        return $this->fetchData($col, $this->query); 
    }

    public function selectByUsername($col, $table_name, $username) {
        $where = 'WHERE username= "'.$username.'"';
        $this->sql = 'select '.$this->splitColumn($col) . ' FROM '. $table_name . ' ' .$where;
        // return $this->sql;
        $this->query = $this->conn->query($this->sql);
        return $this->fetchData($col, $this->query); 
    }
    
    public function deleteUser($id) {
        $id = (int) $id;
        $sql = 'DELETE from tbl_user WHERE id='.$id;
        // return $sql;
        $query = $this->conn->query($sql);
        if($query) {
            return true;
        }
        return false;
    }

    public function updateProfile($id, $username, $email, $password, $nama, $bio) {
        $sql = 'UPDATE '. $this->table .' SET username="'. $username .'", email="'. $email .'", password="'. $password .'", nama="'. $nama .'", bio="'. $bio .'" WHERE id='. $id;
        $status = '';
        if($query = $this->conn->query($sql)){
            echo  "berhasil";
            $this->updateSession($id, $username, $email, $nama, $bio);
            $status = true;
        } else {
            $status = false;
        }
        return $status;
    }
    
    public function updatePhotoProfile($userID, $file) {
        // TODO
        $user = (int) $userID;
        $uploadStatus = $this->uploadProfile($file);
        
        if($uploadStatus == false) {
            return false;
        }
        $pictureName = $uploadStatus;
        $sql = 'UPDATE '. $this->table .' SET profile="'. $pictureName .'" WHERE id='. $user;
        $status = '';
        if($query = $this->conn->query($sql)){
            // echo  "berhasil";
            $this->updateSessionProfile($pictureName);
            $status = $pictureName;
        } else {
            $status = false;
        }
        return $status;
    }

    public function login($username, $password) {
        
        $username = $this->conn->real_escape_string($username);
        $password = $this->conn->real_escape_string($password);
        $data = array();

        $sql = 'select*from ' .$this->table. ' where (username="'. $username.'" or email="'. $username .'") and password = "'. $password .'"';
        // return $sqls;
        if($query = $this->conn->query($sql)) {
            if($query->num_rows > 0) {
                while($row = $query->fetch_assoc()) {
                    // $data['status'] = true;
                    $data['username'] = $row['username'];
                    $data['id'] = $row['id'];
                    $data['email'] = $row['email'];
                    $data['profile'] = $row['profile'];
                    $data['nama'] = $row['nama'];
                    $data['bio'] = $row['bio']; 
                }
                $data['status'] = true;
            } else {
                $data['status'] = false;
            }
        } else {
            $data['status'] = false;
        }

        return $data;
    }

    public function register($username, $email, $password, $file, $nama) {
        
        $uploadProgress = $this->uploadProfile($file);
        // return $uploadProgress;
        if($uploadProgress == false) {
            return false;
            exit;
        }
        $file = $uploadProgress;
        $sql = 'insert into tbl_user (username, password, email, profile, nama) values("'. $username .'", "'. $password .'", "'. $email .'", "'. $file .'", "'. $nama .'")';
            // return $sql;
        if($query = $this->conn->query($sql)) {
            return true;
        } else {
            return false;
        }
        return true;
    }

    public function uploadProfile($file) {
      $currentDir = getcwd();
      $uploadDirectory = '/upload/';
      $fileExtensions = ['jpeg','jpg','png'];
      $fileName = $_FILES[$file]['name'];
      $fileSize = $_FILES[$file]['size'];
      $fileTmpName  = $_FILES[$file]['tmp_name'];
      $fileType = $_FILES[$file]['type'];
      $fileExtension = strtolower(explode('.',$fileName)[1]);
      $newFileName = md5(time() . $fileName) . '.' . $fileExtension;

      $status = false;

      $uploadPath = $currentDir . $uploadDirectory . basename($newFileName); 
    //   return $uploadPath;
    //   echo $uploadPath;

          if (! in_array($fileExtension,$fileExtensions)) {
              // $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
              $status = false;
          }

          if ($fileSize > 2000000) {
              // $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
              $status = false;
          }

          if (empty($errors)) {
              $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

              if ($didUpload) {
                  // echo "The file " . basename($fileName) . " has been uploaded";
                  $status = $newFileName;
              } else {
                  // echo "An error occurred somewhere. Try again or contact the admin";
                  $status = false;
              }
          } else {
              foreach ($errors as $error) {
                  // echo $error . "These are the errors" . "\n";
                  $status = false;
              }
          }
          
          return $status;
    }

  }
?>