<?php
    session_start();
    if(!isset($_SESSION['user_id'])) {
        header('Location: http:/localhost/tugas-akhir/');
    }

    if(isset($_SESSION['user_id']) && isset($_POST['artikel_komen'])) {
        require_once '../Komentar.php';
        $komentar = new Komentar;

        $isiKomen = trim($_POST['artikel_komen']);
        $artikelID = $_POST['artikel_id'];
        $isiKomen = str_replace("\n", "<br>", $isiKomen);
        $userID = $_SESSION['user_id'];

        $isSukses = $komentar->addKomen($artikelID, $userID, $isiKomen);
        if($isSukses) {
            header('Location: http://localhost/tugas-akhir/artikel/?p='. $artikelID);
        } else {
            header('Location: http://localhost/tugas-akhir/artikel/?p='. $artikelID .'&status=1');
            
        }
    }
?>