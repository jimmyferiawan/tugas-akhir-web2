<?php
    session_start();
    if(isset($_POST['idkomen']) && isset($_POST['idartikel']) && isset($_POST['komen_edit']) && isset($_SESSION['user_id'])) {
        $komenID = trim($_POST['idkomen']);
        $isiKomen = trim($_POST['komen_edit']);
        $userID = $_SESSION['user_id'];
        $artikelID = (int) $_POST['idartikel'];
        echo "idkomen: $komenID komen_edit: $isiKomen";
        
        require '../Komentar.php';
        $komen = new Komentar;
        $statusEdit = $komen->editKomen($komenID, $userID, $isiKomen);
        
        if($statusEdit) {
            header('Location: http://localhost/tugas-akhir/artikel?p='.$artikelID);
        } else {
            header('Location: http://localhost/tugas-akhir/artikel?p='.$artikelID).'&status=2';
        }
    }

?>