<?php
    class App {

        public function __construct() {
            $url = $this->parseURL();
            if(count($url) == 0){
                echo 0;
            } else if(count($url) == 1) {
                echo $url[0];
            } else {
                echo 'else';
            }
        }

        public function parseURL() {
            if(isset($_GET['url'])) {
                $url = rtrim($_GET['url'], '/');
                $url = filter_var($url, FILTER_SANITIZE_URL);
                $url = explode('/', $url);
                return $url;
            }
        }
    }

    $app = new App;
?>