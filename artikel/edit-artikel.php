<?php
    if(isset($_POST['judul']) && isset($_POST['submit'])) {
        require_once '../Artikel.php';
        $judul = $_POST['judul'];
        $konten = htmlspecialchars($_POST['konten']);
        $artikelID = (int) $_POST['id'];
        $artikel = new Artikel;
        $hasil = $artikel->editArtikel($artikelID, $judul, $konten);
        if($hasil) {
            header('Location: http://localhost/tugas-akhir/user/?status=3');
        } else {
            header('Location: http://localhost/tugas-akhir/user/user-edit-artikel.php?status=gagal');
        }
        // var_dump($hasil);
    }
?>