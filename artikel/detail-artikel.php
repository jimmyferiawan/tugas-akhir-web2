<?php
    if(!isset($judulArtikel) && !isset($judulArtikel)) {
        echo '404 NOT FOUND!';
        exit;
    }
?>
<div class="artikel-detail">
    <div class="judul-artikel">
        <h5 class="judul-artikel"><?=$judulArtikel?></h5>
        <span class="author-artikel">By: <?= $authorArtikel ?></span>
    </div>
    <div class="konten-artikel">
        <?=$isiArtikel?>
    </div>
</div>