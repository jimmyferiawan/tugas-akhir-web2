<?php
    session_start();
    require_once '../Komentar.php';
    $isLogin = isset($_SESSION['user_id']) ? true : false;
    $kmt = new Komentar;
    $listKomentar = $kmt->getKomenByArtikelID($artikelID);
    // var_dump($listKomentar);
    // exit;
?>
<div class="komen-container row">
    
    <ul class="">
        <?php
            
            if(isset($_SESSION['user_id'])) {
                echo '<li class="add-komentar">
                    <ul class="collection">
                        <li class="collection-item">
                            <form action="http://localhost/tugas-akhir/komentar/add-komentar.php" method="POST">
                                <input type="hidden" name="artikel_id" id="artikel_id" value="' .$artikelID. '">
                                <div class="input-field">
                                    <textarea id="komen" type="text" class="materialize-textarea" name="artikel_komen"></textarea>
                                    <label for="komen">Tambah Komentar</label>
                                </div>
                                <button class="btn" type="submit" name="tambah_komen">Komen</button>
                            </form>
                        </li>
                    </ul>
                </li>';
            }
        ?>
        <?php
            echo '<ul class="collection">';
            foreach($listKomentar as $komen) {
                $authorAction = '';
                if($isLogin) {
                    $isMilikUserLogin = $kmt->cekKomenMilikUser($_SESSION['user_id'], $komen['author']);
                    $authorAction = $isMilikUserLogin ? '<a href="#modal-edit-komen" data-comment="'. $komen['id'] .'" class="modal-trigger trigger-edit" onclick="">Edit</a> &middot; <a href="#" class="red-text">hapus</a>' : '';    
                }
                
                echo '<li class="collection-item avatar">
                    <img class="circle" src="http://localhost/tugas-akhir/user/upload/'. $komen['profile'] .'" alt="" srcset="">
                    <span class="title"><a href="#">'. $komen['username'] .'</a></span>
                    <div class="komen-detail"><span class="detail-tgl">'. $komen['create_time'] .'</span></div>
                    <div class="isi-komen">'. $komen['content'] .'</div>
                    '. $authorAction .'
                </li>';
            }
            echo '</ul>';
        ?>
        
    </ul>
</div>

<div class="modal fixed-footer" id="modal-edit-komen">
    <form action="http://localhost/tugas-akhir/komentar/edit-komentar.php" method="post" id="form-edit-komen">
        <div class="modal-title gradient-1">
            <h4 class="white-text">Edit Komen</h4>
        </div>
        <div class="modal-content">
            <input type="hidden" name="idkomen" id="idkomen">
            <input type="hidden" name="idartikel" id="idartikel" value="<?= $artikelID ?>">
            <div class="input-field">
                <textarea name="komen_edit" id="komen-edit" class="materialize-textarea"></textarea>
                <label for="komen-edit">Edit Komentar</label>
            </div> 
        </div>
        <div class="modal-footer">
            <button type="reset" class="btn red"  name="simpan_komen">Batal</button>
            <button type="submit" class="btn green" name="simpan_komen">Simpan</button>
        </div>
    </form>
</div>
<div class="" id="modal-hapus-komen">
    
</div>
<?php
    if(count($listKomentar > 0)) {
        echo '<script>
            var modalEditKomen = document.getElementById("modal-edit-komen");
            var triggerEdit = document.querySelectorAll(".trigger-edit");
            
            for(var i=0;i < triggerEdit.length;i++) {
                var index = i
                triggerEdit[i].addEventListener("click", function(){
                    document.getElementById("idkomen").value = this.getAttribute("data-comment");
                    console.log(this.getAttribute("data-comment"));
                    document.getElementById("komen-edit").nextElementSibling.classList.add("active");
                    document.getElementById("komen-edit").value = this.previousElementSibling.innerText;
                    console.log(this.previousElementSibling.innerText);
                })
            }
            var isiKomen = document.querySelectorAll(".isi-komen");
            var options = {
                onCloseStart: function() {
                    document.getElementById("form-edit-komen").reset()
                },
                onOpenStart: function() {
                    
                }
            };
            var instanceEditKomen = M.Modal.init(modalEditKomen, options);
        </script>';
    }
?>
        
