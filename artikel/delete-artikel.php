<?php
    session_start();
    if(isset($_POST['post_id'])) {
        require_once '../Artikel.php';
        $artikel = new Artikel;
        $artikelID = $_POST['post_id'];
        $authorID = $_SESSION['user_id'];
        // echo $authorID;
        $hasil = $artikel->deleteArtikel($artikelID, $authorID);
        if($hasil) {
            // var_dump($hasil);
            header('Location: http://localhost/tugas-akhir/user/?status=4');
        } else {
            // var_dump($hasil);
            header('Location: http://localhost/tugas-akhir/user/?status=5');
        }
    }
?>