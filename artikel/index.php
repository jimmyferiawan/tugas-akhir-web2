<?php
    session_start();
    require_once '../Artikel.php';
    $artikel = new Artikel;
    $data = $artikel->getAllArtikel();

    if(isset($_GET['p'])) {
        $id_konten = $_GET['p'];
        $data = $artikel->selectArtikelById($id_konten);
        // validasi error
        if($data == false) {
            echo "404 not found";
            exit;
        }
    }
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="http://localhost/material-icons/iconfont/material-icons.css">
    <link rel="stylesheet" href="http://localhost/tugas-akhir/css/materialize.css">
    <link rel="stylesheet" href="http://localhost/tugas-akhir/css/style.css">
    <script src="http://localhost/tugas-akhir/js/materialize.min.js"></script>
    <title>Document</title>
</head>
<body>
    
    <div class="container white">
        <div class="row">
        <?php
            if(isset($_GET['p'])){
                $judulArtikel = $data['title'];
                $authorArtikel = $data['nama_author'];
                $artikelID = $data['id'];
                $isiArtikel = htmlspecialchars_decode($data['content']);
                require 'detail-artikel.php';
                require 'komentar.php';
            } else {
                foreach($data as $perArtikel) {
                    echo '<ul class="collection col s12 m12 l12 xl12">
                        <li class="collection-item avatar">
                            <img src="http://localhost/tugas-akhir/user/upload/'. $perArtikel['profile'] .'" alt="" class="circle">
                            <span class="title blue-text"><a href="http://localhost/tugas-akhir/artikel/?p='. $perArtikel['id'] .'">'. $perArtikel['title'] .'</a></span>
                            <p>'. $perArtikel['create_time'] .'</p>
                        </li>
                    </ul>';
                }
            }
                
        ?>
        
        </div>
    </div>
    <div id="modal-komentar" class="modal bottom-sheet">
            <div class="modal-content">
                <h4>
                    <?php
                        $textStatus = '';
                        if(isset($_GET['status'])) {
                            $status = $_GET['status'];
                            if($status == 1) {
                                $textStatus = 'Gagal menambahkan komen';
                            }
                            if($status == 2) {
                                $textStatus = 'Gagal mengedit komen';
                            }
                        }
                    ?>
                </h4>
            </div>
    </div>
    <script src="http://localhost/tugas-akhir/js/index.js"></script>
</body>
</html>