<?php
    session_start();
    if(isset($_SESSION['user_id'])) {
        if(isset($_POST['judul']) && isset($_POST['konten'])) {
            $userID = $_SESSION['user_id'];
            require_once '../Artikel.php';
            $artikel = new Artikel;
            echo $userID;
            $judul = htmlspecialchars($_POST['judul']);
            $konten = htmlspecialchars($_POST['konten']); 
            $user = (int) $_SESSION['user_id'];
            $hasil = $artikel->createPost($judul, $konten, $user);
            // exit;
            if($hasil) {
                header('Location: http://localhost/tugas-akhir/user/?status=2');
            } else {
                header('Location: http://localhost/tugas-akhir/user/user-add-artikel.php?msg=1');
            }
        }
    }