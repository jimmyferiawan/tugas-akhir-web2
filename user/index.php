<?php
    session_start();
    if(!isset($_SESSION['username'])) {
        header('Location: http://localhost/tugas-akhir/');
        exit;
    }
    require_once '../Artikel.php';
    $authorID = (int) $_SESSION['user_id'];
    $artikel = new Artikel;
    $data = $artikel->selectArtikelByAuthorId($authorID);
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="http://localhost/material-icons/iconfont/material-icons.css">
    <link rel="stylesheet" href="http://localhost/tugas-akhir/css/materialize.css">
    
    <script src="http://localhost/tugas-akhir/js/materialize.min.js"></script>
    <link rel="stylesheet" href="http://localhost/tugas-akhir/css/style.css">
    <script src="http://localhost/tugas-akhir/js/axios.min.js"></script>
    <title>Profile</title>
    <style>
        html{
            height: 100%;
        }
        body{
            height: 100%;
        }
        .custom-container{
            height: 100%;
        }
        .custom-sidebar{
            display: inline-block;
            vertical-align: top;
            height: 100%;
            overflow-y: auto;
            position: fixed;
            overflow-x: hidden;
            z-index: 999;
            border-right: 1px solid #e0e0e0;
        }
        .custom-content{
            display: inline-block;
            vertical-align: top;
            height: 1500px;
            overflow-y: auto;
            overflow-x: hidden;
        }
    </style>
</head>
<body>

    <div class="row">
        <div class="custom-sidebar col l2 xl2 hide-on-med-and-down  grey lighten-4">
            <div class="row">
                <div class="col l12 xl12">
                    <img class="col l8 xl8 offset-l2 offset-xl2 circle full-width responsive-image" src="http://localhost/tugas-akhir/user/upload/<?=$_SESSION['profile']?>" alt="">
                </div>
                <div class="col l12 xl12">
                   <h5 class="center"><?= $_SESSION['nama'] ?></h5>
                </div>
                <div class="clear-fix"></div>
                <div class="section col l12 xl12 tambahpadding">
                    <a href="http://localhost/tugas-akhir/user/user-add-artikel.php" class=" black-text">
                        <i class="material-icons left">add_circle</i> Add Post
                    </a>
                </div>
                <div class="clear-fix"></div>
                <div class="divider"></div>
                <div class="section col l12 xl12 tambahpadding">
                    <a href="#modal1" class="modal-trigger black-text">
                        <i class="material-icons left">edit</i> Edit Profile
                    </a>
                </div>
                <div class="clear-fix"></div>
                <div class="divider"></div>
                <div class="section col l12 xl12 tambahpadding">
                    <a href="#" class="black-text modal-trigger" data-target="modal-photo">
                        <i class="material-icons left">insert_photo</i> Edit Foto
                    </a>
                </div>
                <div class="clear-fix"></div>
                <div class="divider"></div>
                <div class="section col l12 xl12 tambahpadding">
                    <a href="http://localhost/tugas-akhir/user/user-logout.php" class="black-text">
                        <i class="material-icons left">no_encryption</i> Logout
                    </a>
                </div>
                <div class="clear-fix"></div>
                <div class="divider"></div>
                <div class="section col l12 xl12 tambahpadding">
                    <a href="#" data-target="modal-delete" class="modal-trigger">
                        <i class="material-icons left red-text">delete</i> <span class="black-text">Hapus Akun</span>
                    </a>
                </div>
                <div class="clear-fix"></div>
                <div class="divider"></div>
            </div>
        </div>

        <div class="custom-content col s12 l10 xl10 no-padding right">
            <div class="navbar-fixed">
                <nav class="gradient-1">
                    <div class="nav-wrapper">
                        <a href="#" class="brand-logo">SI-POST</a>
                        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons white-text">menu</i></a>
                    </div>
                </nav>
            </div>
            
            <div class="container" id="main-feed">
                <?php
                    if($data != false) {
                        echo '<ul class="collection col s12 m12 l12 xl12">';
                        foreach($data as $artikel) {
                            echo '<li class="collection-item">
                                
                                    <form action="http://localhost/tugas-akhir/artikel/delete-artikel.php" method="POST">
                                        <div class="right">
                                            <input type="hidden" name="post_id" value="'. $artikel['id'] .'">
                                            <button class="btn-flat no-vertical-align no-padding" type="submit" name="hapus-artikel">
                                                <i class="material-icons red-text">delete</i>
                                            </button>
                                        
                                            <a href="http://localhost/tugas-akhir/user/user-edit-artikel.php?xyz='. $artikel['id'] .'" >
                                                <i class="material-icons green-text text-darken-1">edit</i>
                                            </a>
                                        </div>
                                    </form>
                                
                                <a href="http://localhost/tugas-akhir/artikel/?p='. $artikel['id'] .'" class="blue-text">'. $artikel['title'] .'</a >
                                <div class="grey-text">'. $artikel['create_time'] .'</div>
                            </li>';
                        }
                        echo '</ul>';
                    }
                ?>
            </div>
        </div>
    </div>
    <ul class="sidenav white" id="mobile-demo">
        <li>
            <div class="user-view">
                <img class="circle" src="http://localhost/tugas-akhir/user/upload/<?= $_SESSION['profile'] ?>" alt="">
                <span class="name"><?= $_SESSION['nama'] ?></span>
            </div>
        </li>
        <li>
            <a href="http://localhost/tugas-akhir/user/user-add-artikel.php" class="no-padding">
                <i class="material-icons left margin-right-6">add_circle</i>Add Post
            </a>
        </li>
        <li>
            <a href="#modal1" class="modal-trigger no-padding">
                <i class="material-icons margin-right-6">edit</i>Edit Profile
            </a>
        </li>
        <li>
            <a href="#" data-target="modal-photo" class="modal-trigger no-padding">
                <i class="material-icons margin-right-6">insert_photo</i>Edit Foto
            </a>
        </li>
        <li>
            <a href="http://localhost/tugas-akhir/user/user-logout.php" class="no-padding">
                <i class="material-icons margin-right-6">no_encryption</i>Logout
            </a>
        </li>
        <li>
            <a href="#" data-target="modal-delete" class="modal-trigger no-padding">
                <i class="material-icons red-text margin-right-6">delete</i>Hapus Akun
            </a>
        </li>
    </ul>
    <!-- semua modal -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="row">
            <form action="http://localhost/tugas-akhir/user/edit-profile.php" method="post">
                <div class="modal-content">
                    <h4>Edit Profile</h4>
                    <div class="input-field col s12">
                        <input type="text" id="nama" name="nama" class="validate" value="<?= $_SESSION['nama'] ?>">
                        <label for="nama">Nama</label>
                    </div>
                    <div class="input-field col s12">
                        <input type="text" id="username" name="username" class="validate" value="<?= $_SESSION['username'] ?>">
                        <label for="username">Username</label>
                    </div>
                    <div class="input-field col s12">
                        <input type="email" id="mail" name="email" class="validate" value="<?= $_SESSION['email'] ?>">
                        <label for="email">Email</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="password" id="password" class="validate">
                        <label for="password">Password</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="password" id="password-retype" name="password" class="validate">
                        <label for="password-retype">Ketik ulang password</label>
                    </div>
                    <div class="input-field col s12">
                        <textarea type="text" id="textarea1" name="bio" class="validate materialize-textarea" value="<?= $_SESSION['bio'] ?>"></textarea>
                        <label for="textarea1">Bio</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn waves-effect teal lighten-1" name="simpan_profile">Simpan</button>
                    <button type="reset" class="btn modal-close waves-effect red">Batal</button>
                </div>
            </form>
        </div>
    </div>
    <div id="modal-delete" class="modal">
        <div class="modal-content">
        <h4>Apkah anda yakin ingin menghapus akun ?</h4>
        </div>
        <div class="modal-footer">
            <a href="http://localhost/tugas-akhir/user/user-delete.php" class="btn teal lighten-1">Hapus</a>
            <a href="#" class="btn modal-close red">Batal</a>
        </div>
    </div>
    <div class="modal" id="modal-pesan">
        <div class="modal-content">
            <h5 class="white-text" id="modal-pesan-text"></h5>
        </div>
    </div>
    <div class="modal modal-fixed-footer" id="modal-photo">
        <form id="form-edit-photo" action="http://localhost/tugas-akhir/user/user-edit-photo.php" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="row">
                    <div class="col s2 m4 l4 xl4 offset-s5 offset-m4 offset-s8 offset-s8">
                        <img id="edit-preview" class="responsive-image full-width circle" src="http://localhost/tugas-akhir/user/upload/<?= $_SESSION['profile'] ?>" alt="">
                    </div>
                    <div class="file-field input-field col s12">
                            <i class="material-icons prefix"></i>
                            <div class="btn deep-purple darken-1">
                                <span>Foto</span>
                                <input type="file" id="profile" name="profile" accept="image/png,image/jpeg">
                            </div>
                            <div class="file-path-wrapper">
                                <input type="text" class="file-path validate" placeholder="Upload foto profil" readonly>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="btn_edit_photo" class="btn teal lighten-1">Simpan</button>
                <a href="#" class="btn modal-close red">Batal</a>
            </div>
        </form>
    </div>
    
    <script src="http://localhost/tugas-akhir/js/index.js"></script>
    <?php
        if(isset($_GET['status'])) {
            $status = (int) $_GET['status'];
            $pesan = array(
                'Koooosong!!??',
                'Gagal Hapus Akun',
                'Berhasil menambah artikel',
                'Berhasil edit artikel',
                'Berhasil delete artikel',
                'Gagal delete artikel',
                'Berhasil update Photo Profile',
                'Gagal update Photo Profile'
            );
            $pesanText = '';
            $backgroundModal = '';
            switch($status){
                case 1:
                    $pesanText = $pesan[1];
                    $warna = 'red';
                    break;
                case 2:
                    $pesanText = $pesan[2];
                    $warna = 'green';
                    break;
                case 3:
                    $pesanText = $pesan[3];
                    $warna = 'green';
                    break;
                case 4:
                    $pesanText = $pesan[4];
                    $warna = 'green';
                    break;
                case 5:
                    $pesanText = $pesan[5];
                    $warna = 'red';
                    break;
                case 6:
                    $pesanText = $pesan[6];
                    $warna = 'green';
                    break;
                case 7:
                    $pesanText = $pesan[7];
                    $warna = 'red';
                    break;
                default:
                    $pesanText = $pesan[0];
                    $warna = 'red';
                    break;
            }
            echo '<script>
                var modalPesan = document.getElementById("modal-pesan");
                modalPesan.classList.add("'. $warna .'");
                var modalPesanText = document.getElementById("modal-pesan-text");
                modalPesanText.innerText = "'. $pesanText .'";
                var modalPesanInstance = M.Modal.getInstance(modalPesan);
                modalPesanInstance.open();
            </script>';
        }
    ?>
    <script>
        // var mainFeed = document.getElementById('main-feed');
        // axios.get('http://localhost/tugas-akhir/user/list-user-post.php')
        //     .then(function(res){
        //         mainFeed.innerHTML = res.data;
        //     })
        //     .catch(function(err){
        //         console.log('error');
        //     });
        
        var editPreview = document.getElementById('edit-preview');
        var originalPhoto = editPreview.src;
        var modalEditPhoto = document.getElementById('modal-photo');
        var profile = document.getElementById('profile');
        var options = {
            onCloseEnd: function(){
                console.log('form closed')
                document.getElementById('form-edit-photo').reset()
                document.getElementById('edit-preview').src = originalPhoto
            }
        }
        var instanceEditPhoto = M.Modal.init(modalEditPhoto, options);

        profile.addEventListener('change', function(event) {
            var reader = new FileReader()
            reader.onload = function() {
                var output = document.getElementById('edit-preview')
                output.src = reader.result
            }
            reader.readAsDataURL(event.target.files[0])
        })
    </script>
</body>
</html>