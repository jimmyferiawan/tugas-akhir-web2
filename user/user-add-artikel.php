<?php
    session_start();
    if(!isset($_SESSION['username'])) {
        header('Location: http://localhost/tugas-akhir');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="http://localhost/tugas-akhir/css/quill.snow.css">
    <link rel="stylesheet" href="http://localhost/tugas-akhir/css/materialize.css">
    <link rel="stylesheet" href="http://localhost/tugas-akhir/css/style.css">
    <script src="http://localhost/tugas-akhir/js/materialize.min.js"></script>
    <script src="http://localhost/tugas-akhir/js/axios.min.js"></script>
    <title></title>
    <style>
        
    </style>
</head>
<body>
    <div class="navbar-fixed">
        <nav class="nav-extended indigo lighten-1">
            <div class="container nav-wrapper">
                <a href="#" class="brand-logo left ">SI-POST</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down black-text">
                    <li>
                        <form action="">
                            <div class="input-field">
                                <input type="text">
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="row">
        
    </div>
    <div class="row">
        <div class="container">
            <form action="http://localhost/tugas-akhir/artikel/create-artikel.php" method="post" id="myform">
                <div class="input-field">
                    <input type="text" id="judul" name="judul">
                    <label for="judul">judul</label>
                    <span class="helper-text" data-error="judul tidak boleh kosong"></span>
                </div>
                <div id="container"></div>
                <div id="editor" class="add-margin-bottom-12"></div>
                <textarea name="konten" id="konten"></textarea>
                <!-- <div class="input-field">
                    <input type="text" id="tag" name="tag">
                    <label for="tag">tag</label>
                </div> -->
                
                <button type="submit" name="submit" class="col s12 m10 l8 xl8 offset-m1 offset-l2 offset-xl2 btn round-btn gradient-1">submit</button>
            </form>
            <?php
                if(isset($_GET['status'])){
                    $status = $_GET['status'];
                    if($status == 'gagal') {
                        echo '<h5 class="red-text col s12 center">gagal post</h5>';
                    } 
                }
            ?>
        </div>
        <!-- <div class="fixed-action-btn">
            <button class="btn-floating btn-large indigo accent-2" id="post-artikel">simpan</button>
        </div> -->
    </div>

    <script src="http://localhost/tugas-akhir/js/quill.js"></script>
    <script src="http://localhost/tugas-akhir/js/index.js"></script>
    <script>
        var toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike', 'image', 'link'],
            ['blockquote', 'code-block'],
            [{'list': 'ordered'}, {'list': 'bullet'}],
            [{'header': [1,2,3,4,5,6, false]}],
            ['clean']
        ]
        var editor = new Quill('#editor', {
            theme: 'snow',
            modules: {
                toolbar: toolbarOptions
            }
        })
        
        // var btnPostArtikel = document.getElementById('post-artikel')
        var tool = document.getElementById('editor')
        var konten = document.getElementById('konten')
        var myForm = document.getElementById('myform')

        // btnPostArtikel.addEventListener('click', function() {
        //     document.getElementById('myform').submit()
        // }) 

        myForm.addEventListener('submit', function(event){
            var hasil = editor.root.innerHTML
            konten.value = hasil
            console.log(konten.value)
            if(konten.value == '') {
                event.preventDefault()
                return false
            } else {
                return true
            }
        })

        
    </script>
</body>
</html>