<?php
    session_start();
    if(isset($_SESSION['user_id'])) {
        if(isset($_POST['simpan_profile'])) {
            require_once '../User.php';
            $user = new User;

            $user_id = $_SESSION['user_id'];
            $nama =  htmlspecialchars($_POST['nama']);
            $username =  htmlspecialchars($_POST['username']);
            $email =  htmlspecialchars($_POST['email']);
            $password =  htmlspecialchars($_POST['password']);
            $bio =  htmlspecialchars($_POST['bio']);

            $user->updateProfile($user_id, $username, $email, $password, $nama, $bio);
        }
    } else {
        header('Location: http://localhost/tugas-akhir/');
        exit;
    }
    
?>
