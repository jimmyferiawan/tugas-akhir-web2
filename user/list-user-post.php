
<?php
    session_start();
    $authorID = "";
    if(isset($_SESSION['user_id'])) {
        $authorID = (int) $_SESSION['user_id'];
    } else {
        echo 'error';
        exit;
    }
    
    require_once '../Artikel.php';
    $artikel = new Artikel;
    $data = $artikel->selectArtikelByAuthorId($authorID);
    // var_dump($data);
    // exit;
    if($data != false) {
        echo '<ul class="collection col s12 m12 l12 xl12">';
        foreach($data as $artikel) {
            echo '<li class="collection-item">
                <div class="right">
                    <a href="#!" >
                        <i class="material-icons red-text">delete</i>
                    </a> 
                    <a href="#!" >
                        <i class="material-icons green-text text-darken-1">edit</i>
                    </a>
                </div>
                <a href="#" class="blue-text">'. $artikel['title'] .'</a >
                <div>'. $artikel['create_time'] .'</div>
            </li>';
        }
        echo '</ul>';
    } 
?>