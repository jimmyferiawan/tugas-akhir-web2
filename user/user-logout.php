<?php

    session_start();
    if(isset($_SESSION['user_id'])) {
        session_destroy();
        header('Location: http://localhost/tugas-akhir/?status=3');
    } else {
        header('Location: http://localhost/tugas-akhir/');
    }